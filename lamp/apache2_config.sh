# Configuration Apache
cat << EOF > /etc/apache2/sites-available/vagrant.conf
<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	DocumentRoot /www
	ErrorLog /var/log/apache2/vagrant_error.log
	CustomLog /var/log/apache2/vagrant_access.log combined
	<Directory /www>
		AllowOverride All
		Require all granted
	</Directory>
</VirtualHost>
EOF

a2dissite 000-default
a2ensite vagrant
a2enmod rewrite

/etc/init.d/apache2 restart
